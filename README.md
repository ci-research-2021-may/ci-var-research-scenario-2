# 👋 Hi
Thank you for participating in the research to evaluate the GitLab CI/CD variables offerings. The instructions for the activity are as follows:

## 📝  Instructions
In this scenario, you are a software engineer at a startup. The test system generates a set of credentials (username and password) for running integration tests in a separate environment and stores these in a dotenv file called `.credentials.env`. For security purposes, the credentials are rotated every hour. Your manager has requested that you send the credentials for the testing environment (created by this project's pipeline) to [**a downstream pipeline**](https://gitlab.com/ci-research-2021-may/ci-var-research-scenario-2-b) in order to run the additional integration tests.


## 🔗 Link to downstream project
https://gitlab.com/ci-research-2021-may/ci-var-research-scenario-2-b/-/tree/master 

## ℹ️ Documetation links
Read more about:
1. [dotenv variable inheritance](https://docs.gitlab.com/ee/ci/variables/README.html#pass-an-environment-variable-to-another-job)
2. [Cross project artifact downloads](https://docs.gitlab.com/ee/ci/yaml/README.html#cross-project-artifact-downloads-with-needs)
3. [Pass variables to a downstream pipeline with dotenv variable inheritance and cross project artifact downloads](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html#with-variable-inheritance)







